<?php
// Datos de configuracion

// Datos del administrador
$root = "root";
$passwd_admin = "root";

// Configuracion twig
require_once realpath(dirname(__FILE__) . 
					"/../vendor/twig/twig/lib/Twig/Autoloader.php");
				
Twig_Autoloader::register();

# creamos instancia del cargador 
$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__) .
											"/../vistas"));
											
$twig = new Twig_Environment($loader);


$con = new PDO('sqlite:Recetario.db');

?>